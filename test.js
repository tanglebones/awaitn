const awaitn = require('./index');

const requestsToMake = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g' ];
async function doRequest(r) { return Date.now() + ' r: ' + r; }

function* dgmap(iterable, f) {
  for (const n of iterable) {
    yield () => f(n);
  }
} 

// at most 4 in flight at a time, max 1 request every 200ms.
awaitn(4, dgmap(requestsToMake, doRequest), console.log, console.error, 1, 200);

async function t() {
  {
    function* simple() {
      yield Promise.resolve(1);
      yield Promise.resolve(2);
      yield Promise.reject(3);
      yield Promise.resolve(4);
    }

    async function doN(i, mpp = undefined, tp = undefined) {
      try {
        await awaitn(
          i,
          simple(),
          res => console.log(Date.now(), `simple resolved: ${res}`),
          rej => console.error(Date.now(), `simple rejected: ${rej}`),
          mpp,
          tp
        );
        console.log(Date.now(), 'simple', 'ok', i)
      } catch (e) {
        console.error(e);
      }
    }

    async function test() {
      for (let i = 1; i < 6; ++i) {
        console.log(Date.now(), 'simple', i);
        await doN(i);
        await doN(i, 1);
        await doN(i, 1, 100);
      }
    }

    try {
      await test();
      console.log(Date.now(), 'done simple');
    } catch (e) {
      console.error(e);
    }
  }
  {
    function* deferred() {
      yield () => Promise.resolve(1);
      yield () => Promise.resolve(2);
      yield () => Promise.reject(3);
      yield () => Promise.resolve(4);
    }

    async function doN(i, mpp = undefined, tp = undefined) {
      try {
        await awaitn(
          i,
          deferred(),
          res => console.log(Date.now(), `deferred resolved: ${res}`),
          rej => console.error(Date.now(), `deferred rejected: ${rej}`),
          mpp,
          tp
        );
        console.log(Date.now(), 'deferred', 'ok', i)
      } catch (e) {
        console.error(e);
      }
    }

    async function test() {
      for (let i = 1; i < 6; ++i) {
        console.log(Date.now(), 'deferred', i);
        await doN(i);
        await doN(i, 1);
        await doN(i, 1, 100);
      }
    }

    try {
      await test();
      console.log(Date.now(), 'done deferred');
    } catch (e) {
      console.error(e);
    }
  }
}

t();