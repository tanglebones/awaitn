const rr_res = 1;
const rr_rej = 2;

module.exports = async function awaitn(n, generator, onResolve = undefined, onReject = undefined, maxPerPeriod = undefined, timePeriodInMilliseconds = undefined) {
  const runningPromises = [];
  let timeStamps = [];
  let deferredGeneration;

  if (maxPerPeriod) {
    timePeriodInMilliseconds = timePeriodInMilliseconds || 1000; // default period is 1 second
  }

  function seti(p, i) {
    runningPromises[i] = (async () => {
      let res;
      let rej;
      let rr;
      try {
        rr = rr_res;
        res = await (deferredGeneration ? p() : p);
      } catch (e) {
        rr = rr_rej;
        rej = e;
      }
      return { i, res, rej, rr };
    })();
  }

  let nextPromise;
  let outstanding = 0;

  // pull up to n promises to wait on from the generator.
  let preSpawn = n;
  if (maxPerPeriod && maxPerPeriod < n) {
    preSpawn = maxPerPeriod;
  }

  for (let i = 0; i < preSpawn; ++i) {
    nextPromise = generator.next().value;
    if (deferredGeneration === undefined) {
      // first generated value is used to determine if differred generation is being used.
      deferredGeneration = typeof nextPromise === 'function';
    }
    if (!nextPromise) { break; }
    outstanding += 1;

    if (maxPerPeriod) {
      timeStamps.push(Date.now());
    }

    seti(nextPromise, i);
  }

  let done = false;

  for (; ;) {
    // if we are done we have to filter out the completed promises
    let stillRunning = done ? runningPromises.filter(x => x) : runningPromises;

    // wait from one to finish.
    let completed = await Promise.race(stillRunning);
    runningPromises[completed.i] = null;

    if (completed.rr === rr_res && onResolve) {
      let x = onResolve(completed.res);
      if (x) { return x; }
    } else if (completed.rr === rr_rej && onReject) {
      let x = onReject(completed.rej);
      if (x) { return x; }
    }

    async function rateLimit() {
      const now = Date.now();
      const expireBefore = now - timePeriodInMilliseconds;
      timeStamps = timeStamps.filter(x => x > expireBefore);
      if (timeStamps.length >= maxPerPeriod) {
        // wait for earliest issue time to fall out of the timePeriodInMilliseconds
        const timeout = timeStamps[0] - expireBefore;
        await new Promise(res => setTimeout(res, timeout)).catch(() => { });
      }
      timeStamps.push(Date.now());
    }

    if (!done) {
      // if the generator schedules the promise right away we had to delay before calling it
      // this can result in a delay after the final promise has been returned because we have to
      // read the generator past the end of the stream to determine it is done
      if (maxPerPeriod && !deferredGeneration) {
        await rateLimit();    
      }

      // more?
      const next = generator.next();
      nextPromise = next.value;
      done = next.done;
    }

    if (done) {
      outstanding -= 1;
      if (outstanding === 0) {
        // no more to wait on
        return;
      }
    } else {
      // if the generator returns a function that schedule the promise we can move the delay
      // to after we've pulled from the generator to avoid a stall after running the generator
      // dry.
      if (maxPerPeriod && deferredGeneration) {
        await rateLimit();    
      }
      // reuse the slot
      seti(nextPromise, completed.i);
    }
  }
}

